ds_list_clear(global.dialogueText);
ds_list_clear(global.dialogueChar);

switch(argument0) {

    case "test":
    
        ds_list_add(global.dialogueText, "This is just a test. Is it working?");
   
    break;

    case "d101":
    
        // Home
        ds_list_add(global.dialogueText, "Aaah!");
        ds_list_add(global.dialogueChar, "mainChar");    
        
        ds_list_add(global.dialogueText, "Look, man. Wake up!");
        ds_list_add(global.dialogueChar, "mikeChar");
        
        ds_list_add(global.dialogueText, "Five more minutes. Geez!");
        ds_list_add(global.dialogueChar, "mainChar");

        ds_list_add(global.dialogueText, "No, man. You overslept already, man. Wake up, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
                
        ds_list_add(global.dialogueText, "Dammit. I'm not gonna get my sleep even after all of this.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Man must be well rested before he dies, Mike!");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Don't be stupid, man. And wake up, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
        
        ds_list_add(global.dialogueText, "Pick up the stuff and meet us outside, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
        
        ds_list_add(global.dialogueText, "Alright, alright. I'll be there in few minutes.");
        ds_list_add(global.dialogueChar, "mainChar");
        
    break;
        
    case "d102":
        
        // Leaving home.
        ds_list_add(global.dialogueText, "Sup, John. You look lost. I told you to go to sleep early.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Hey. Well, I think I got everything anyway.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Great, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
        
        ds_list_add(global.dialogueText, "Let's go, then.");
        ds_list_add(global.dialogueChar, "paulChar");
        
    break;
        
    case "d103":
        
        // Forest
        ds_list_add(global.dialogueText, "Guys, do you really think it will work?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "I think it's nonsense. That stuff doesn't exist.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Don't be so negative, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
        
        ds_list_add(global.dialogueText, "Exactly. I heard it's unreal stuff, literally!");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Look at Mike, he believes and is more open minded than you.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "No, man. I think it's bullshit, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
        
        ds_list_add(global.dialogueText, "But at least I'm gonna die happily, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
    
        ds_list_add(global.dialogueText, "I...");
        ds_list_add(global.dialogueChar, "paulChar");

        ds_list_add(global.dialogueText, "I should have left you two assholes at home and just let you die.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "When is your girlfriend coming, John?");
        ds_list_add(global.dialogueChar, "paulChar");
 
        ds_list_add(global.dialogueText, "Later today. I hope she will be able to find us.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Don't worry, it's very simple road. I mean, it's straight.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Can't get lost, really.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Hm... if you say so.");
        ds_list_add(global.dialogueChar, "mainChar");
        
    break;  
    
    case "d104":
        
        // Arrived
        ds_list_add(global.dialogueText, "Are we there yet?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Not sure, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
        
        ds_list_add(global.dialogueText, "No, not there yet, guys. What are you, a little children?");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "I'm just asking. Is that a crime?");
        ds_list_add(global.dialogueChar, "mainChar");
        
    break;
    
    case "d105":
        
        // Arrived
        ds_list_add(global.dialogueText, "Alright, guys. We're here. Let's rest a little.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "We will need a lot of energy later.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "I slept only four hours. I agree.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Great, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
        
    break;
    
    case "d106":
        
        // Fence        
        ds_list_add(global.dialogueText, "I should go back to guys.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Looks like the guy making this fence wasn't paid very well.");
        ds_list_add(global.dialogueChar, "mainChar");
        
    break;
    
    case "d201":
        
        // Defective by design
        ds_list_add(global.dialogueText, "So, Paul, what is that stuff you want to save the world with?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "It's some new type of drug. But it's not meaningless like rest of them.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "I heard it helps you connect with higher beings.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Sure, sure, higher beings. So it's LSD?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "No, nothing like that. It will shoot you to another dimension.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Dimension we can't comprehend right now.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "And how is that supposed to help us? By hallucinating when we die?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "No, we will connect with those higher beings which are living there.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "And we will ask them for help. And they will help us. Duh.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "... are you actually serious? That's the stupidest thing I have ever heard.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "If this is your plan, then, fuck me.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "No, you don't understand. It's not just some drug");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "I heard that molecules of this stuff are in every human and animal.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Just in very small doses. But if you take a lot of it, you will be");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "able to connect with all living beings on this planet.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "On a level we don't understand yet.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Paul, that is the most idiotic thing I have ever heard.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "But whatever, we're here, let's have fun.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "That radioactive cloud will come in few days, so let's enjoy our last days.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "I picked some good drinks.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "You will see. You are wrong. I can feel it. But yeah, let's have fun, too.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Well, let's get ready. I will find some wood.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "You guys prepare the tent and drinks.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Sure. Just pick the fallen branches from the ground.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "We have an axe, but you know, that would be work.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Alright. Oh and wake Mike up, he's still sleeping.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Oh... yeah. I didn't even notice he was still sleeping.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Our little princess. But every minute without hearing 'man' is treasured.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Anyway, I'm leaving. I'll be back shortly.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Alright. See you later.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "I'm not sleeping, man. I heard everything, man.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Just wait before I manage to wake up, man.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Pfff.");
        ds_list_add(global.dialogueChar, "mainChar");
        
    break;  
    
    case "d202":
        
        // In the woods
        ds_list_add(global.dialogueText, "I can't believe Paul persuaded me into doing this.");
        ds_list_add(global.dialogueChar, "mainChar");

        ds_list_add(global.dialogueText, "He's an idiot. I should have stayed at home playing videogames.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "And god knows if my girlfriend will find us here.");
        ds_list_add(global.dialogueChar, "mainChar");
        
    break;
    
    case "d203":
        
        // Woods completed
        ds_list_add(global.dialogueText, "Alright, that's enough. Will keep us warm for a while.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Let's go back and see what those two are doing.");
        ds_list_add(global.dialogueChar, "mainChar");
                
    break;
    
    case "d204":
        
        // Bringing the stuff back
        ds_list_add(global.dialogueText, "I see you are ready, guys. Didn't expect that. Nice.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Sure we are, man.");
        ds_list_add(global.dialogueChar, "mikeChar");
        
        ds_list_add(global.dialogueText, "Bla bla bla. Bring the wood here, big guy.");
        ds_list_add(global.dialogueChar, "paulChar");
                
    break;
    
    case "d205":
        
        // What now?
        ds_list_add(global.dialogueText, "Nice. Warm and nice. So what now?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "What do you think? Sit down and let's go.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "We don't have that much time left.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Paul...");
        ds_list_add(global.dialogueChar, "mainChar"); 

        ds_list_add(global.dialogueText, "Um... nevermind. Alright, let's.");
        ds_list_add(global.dialogueChar, "mainChar");
                
        ds_list_add(global.dialogueText, "Then everybody get the paper and eat it.");
        ds_list_add(global.dialogueChar, "paulChar");            
                
    break;
    
    case "d206":
        
        // Start
        ds_list_add(global.dialogueText, "Duuude. How many of them are there? Those bags are FULL.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Where did you get it?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "I have my sources. Shut up and eat.");
        ds_list_add(global.dialogueChar, "paulChar");
        
        ds_list_add(global.dialogueText, "Alright. How fast we will be in... um, different dimension?");
        ds_list_add(global.dialogueChar, "mainChar"); 
        
        ds_list_add(global.dialogueText, "I don't know. We will see, I guess.");
        ds_list_add(global.dialogueChar, "paulChar");              
                
        ds_list_add(global.dialogueText, "Oooh... I... I think it's kicking in.");
        ds_list_add(global.dialogueChar, "mainChar")
        
    break;
    
    case "d301":
        
        // Start
        ds_list_add(global.dialogueText, "Oh, nice. That's gonna be beautiful!");
        ds_list_add(global.dialogueChar, "mainChar");
                
    break;    
    
    case "d302":
        
        // Start
        ds_list_add(global.dialogueText, "Shit.");
        ds_list_add(global.dialogueChar, "mainChar");
                
    break;   
    
    case "d303":
        
        // First contact
        ds_list_add(global.dialogueText, "OH WHAT THE F");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Greetings, wanderer.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "Oh shit, what the fuck!");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Very disrespectful, wanderer.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "However, I must ask you, why did you come here?");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "What are you seeking?");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "Well... my friend gave me something and...");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Oh god, where am I?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "You must find out yourself. However, I like to guide lost souls.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "I am willing to be your guide in this world.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "Oh... alright. Great. So, how do I get back?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "When you fulfill your purpose.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "Alright... what is my purpose?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Nobody but you can answer this question.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "You must know your purpose deep in your heart.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "I don’t want to be disrespectful, but you are not helpful at all.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Then, wanderer, I shall leave you alone.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "You must find your purpose yourself only.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "We shall meet again when you are ready.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "No, wait!");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Farewell, wanderer.");
        ds_list_add(global.dialogueChar, "guideChar");
        
        ds_list_add(global.dialogueText, "Well, brilliant.");
        ds_list_add(global.dialogueChar, "mainChar");
                
    break;  
    
    case "d304":
        
        // New friends
        ds_list_add(global.dialogueText, "Fascinating. Simply fascinating.");
        ds_list_add(global.dialogueChar, "alkiviadisChar");
        
        ds_list_add(global.dialogueText, "Hi. What is fascinating?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Simply amazing. Unbelievable.");
        ds_list_add(global.dialogueChar, "alkiviadisChar");
        
        ds_list_add(global.dialogueText, "But WHAT?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Trust amazing Spyros. Don’t believe despicable Dagon.");
        ds_list_add(global.dialogueChar, "alkiviadisChar");
        
        ds_list_add(global.dialogueText, "Alright?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Splendid. Amazing.");
        ds_list_add(global.dialogueChar, "alkiviadisChar");
        
        ds_list_add(global.dialogueText, "You are like a jammed machine. Bye.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Great. See you later, wanderer.");
        ds_list_add(global.dialogueChar, "alkiviadisChar");
                
    break;
    
    case "d305":
        
        // New friends
        ds_list_add(global.dialogueText, "Firstly, he wanted to take her.");
        ds_list_add(global.dialogueChar, "annChar");
        
        ds_list_add(global.dialogueText, "Who wanted to take who?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Then, he said he would take her.");
        ds_list_add(global.dialogueChar, "annChar");
        
        ds_list_add(global.dialogueText, "Take who?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Then, he took her.");
        ds_list_add(global.dialogueChar, "annChar");
        
        ds_list_add(global.dialogueText, "Who?");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Now, she is gone.");
        ds_list_add(global.dialogueChar, "annChar");
        
        ds_list_add(global.dialogueText, "Don’t worry. I’m sure it’s gonna be alright.");
        ds_list_add(global.dialogueChar, "mainChar");
        
        ds_list_add(global.dialogueText, "Another ruined life. Purposeless waste.");
        ds_list_add(global.dialogueChar, "annChar");
        
        ds_list_add(global.dialogueText, "Because of you, Dagon. I hate you.");
        ds_list_add(global.dialogueChar, "annChar");
        
        ds_list_add(global.dialogueText, "Hate is not good. Anyway, I have to go. Bye.");
        ds_list_add(global.dialogueChar, "mainChar");
           
    break; 
    
    case "d306":
    
        ds_list_add(global.dialogueText, "Greetings, wanderer. Do you like what do you see?");
        ds_list_add(global.dialogueChar, "dagonChar")
        
        ds_list_add(global.dialogueText, "Not really."); 
        ds_list_add(global.dialogueChar, "mainChar")
        
        ds_list_add(global.dialogueText, "That is unfortunate. However, I can help you get away. My name Dagon.");
        ds_list_add(global.dialogueChar, "dagonChar")
        
        ds_list_add(global.dialogueText, "Yeah? How can you help me?");
        ds_list_add(global.dialogueChar, "mainChar")
        
        ds_list_add(global.dialogueText, "I can get you back where you came from.");
        ds_list_add(global.dialogueChar, "dagonChar")
        
        ds_list_add(global.dialogueText, "Really? How?");
        ds_list_add(global.dialogueChar, "mainChar")
        
        ds_list_add(global.dialogueText, "Is it simple. Keep going. You will meet Spyros. Don’t trust anything he says.");
        ds_list_add(global.dialogueChar, "dagonChar")
        
        ds_list_add(global.dialogueText, "Ignore him. Then keep going. You will arrive at the cliff. Jump off the cliff.");
        ds_list_add(global.dialogueChar, "dagonChar")
        
        ds_list_add(global.dialogueText, "WHAT? How is that supposed to save me?");
        ds_list_add(global.dialogueChar, "mainChar")
        
        ds_list_add(global.dialogueText, "It is a test. You see, there is a portal down there. You won’t die.");
        ds_list_add(global.dialogueChar, "dagonChar")
        
        ds_list_add(global.dialogueText, "Alright. Well then, we will see. Maybe I don’t hate it here that much after all.");
        ds_list_add(global.dialogueChar, "mainChar")
        
        ds_list_add(global.dialogueText, "You don’t belong here.");
        ds_list_add(global.dialogueChar, "dagonChar")
        
        ds_list_add(global.dialogueText, "I guess. Anyway, I am leaving. See you later... maybe.");
        ds_list_add(global.dialogueChar, "mainChar")
        
        ds_list_add(global.dialogueText, "We shall see."); 
        ds_list_add(global.dialogueChar, "dagonChar")
    
    break;  
    
}
