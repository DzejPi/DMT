/// Set global variables
parallaxChill = 10;
global.textOver = false;
global.destroySelf = false;

global.walkingSpeed = 1;
global.whichDialogue = "";
global.characterState = "idle";
global.characterDisabled = 0;

global.MikeCharacterState = "idle";
global.PaulCharacterState = "idle";
global.EveCharacterState = "idle";

global.dialogueText = ds_list_create();
global.dialogueChar = ds_list_create();

global.language = "ENG";

global.parallax1 = 1/parallaxChill;
global.parallax2 = 2/parallaxChill;
global.parallax3 = 1.5/parallaxChill;
global.parallax4 = 4/parallaxChill;
global.parallax5 = 5/parallaxChill;

global.devilSpeed = 1/4;

global.activeMenu = "main";

